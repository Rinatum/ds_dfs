# DS_DFS

### codes
0 - success
1 - no requested item
2 - wrong path
3 - no connection to datanode
    
    
### namenode methods
## Client interface : 

* `погнали`
* `создать_файлик filepath`
* `читать_файлик filepath`
* `писать_файлик filepath`
* `убить_файлик filepath`
* `че_за_файлик filepath`
* `копи_файлик source_path dest_path`
* `сдвинуть_файлик source_path dest_path`
* `идти_в_папку dirpath`
* `че_в_папке dirpath`
* `создать_папку dirpath`
* `убить_папку firpath`


    init(): code, free space in dfs
    
    Folders
    
    dir_create(path_to_dir, dir_name): code, dir name
    dir_read(path_to_dir): code, dir content
    dir_delete(path_to_dir, dir_name): code 
    
    Files
    
    file_create(path_to_file, file_name): code
    file_info(path_to_file, file_name): code, file_info in str
    file_delete(path_to_file, file_name): code
    file_copy(path_to_file, file_name, path_to_new_dir): code
    file_move(path_to_file, file_name, path_to_new_dir): code
    file_write(path_to_dir, file_name, file_bytes): code
    file_read(path_to_dir, file_name): code, fily in bytearray
    
    
## Deployment

* create aws instances and install docker + docker-compose + git to each one
* `docker swarm init`
* `docker sworm join <token>` (given by docker swarm init)
* `docker stack deploy --compose-file docker-compose.yaml ds_dfs`
* `Clone repo on your local machine`
# namenode:
    image: simonwt/ds_python_dfs:namenode
    volumes:
      - ./src:/app/src
    deploy:
      replicas: 1
      placement:
        constraints:
           - node.id == yqocne4ukmuvty3tuajepwrfz YOUR NODE.ID HERE
    ports:
      - target: 55555
        published: 55555`
* `build client container: docker-compose -f docker-compose.client.yaml up --build`
* ssh to client container and run `погнали`
    