import os
import rpyc
from rpyc import ThreadedServer

codes = {0: "success",
         1: "no requested item",
         2: "wrong path",
         3: "no connection to datanode"}

CURRENT_DIR = ['/']


class Client(rpyc.Service):
    def __init__(self):
        self.connection = rpyc.connect("3.134.101.132", 55555)
        super(Client, self).__init__()

    def full_path(self, path):
        if path[0] == '/':
            return path
        elif '..' in path:
            current_dir = CURRENT_DIR.copy()
            for dir in path.split('/'):
                if dir == '..':
                    current_dir.pop(-1)
                else:
                    current_dir.append(dir)
            return os.path.join(*current_dir)
        elif path[0] == '.':
            return os.path.join(*CURRENT_DIR)
        else:
            return os.path.join(*(CURRENT_DIR + path.split('/')))

    def filepath_filename(self, path):
        abs_path = self.full_path(path)
        path_files = abs_path.split('/')
        filepath, filename = os.path.join(*path_files[:-1]), path_files[-1] if path_files[-1] else path_files[0]
        return '/' + filepath if filepath else '/', filename

    # API
    def exposed_initialize(self):
        available_size = self.connection.root.init()
        return available_size

    def exposed_create_file(self, path):
        path_to_file, file_name = self.filepath_filename(path)
        code = self.connection.root.file_create(path_to_file, file_name)
        return codes[code]

    def exposed_read_file(self, path):
        path_to_file, file_name = self.filepath_filename(path)
        code, result = self.connection.root.file_read(path_to_file, file_name)
        if code == 0:
            return codes[code], result
        else:
            return codes[code], 'error'

    def exposed_write_file(self, local_path, remote_path):
        if os.path.isfile(local_path):
            with open(local_path, 'rb') as f:
                content = f.read()

                path_to_file, file_name = self.filepath_filename(remote_path)
                code = self.connection.root.file_write(path_to_file, file_name, content)
                return codes[code]

        else:
            return 'File' + local_path + 'does not exist'

    def exposed_delete_file(self, path):
        path_to_file, file_name = self.filepath_filename(path)
        code = self.connection.root.file_delete(path_to_file, file_name)
        return codes[code]

    def exposed_get_file_info(self, path):
        path_to_file, file_name = self.filepath_filename(path)
        code, result = self.connection.root.file_info(path_to_file, file_name)
        if code == 0:
            return result
        else:
            return codes[code]

    def exposed_copy_file(self, source_path, dest_path):
        path_to_file, file_name = self.filepath_filename(source_path)
        remote_dest_path = self.full_path(dest_path)

        code = self.connection.root.file_copy(path_to_file, file_name, remote_dest_path)
        return codes[code]

    def exposed_move_file(self, source_path, destination_path):
        path_to_file, file_name = self.filepath_filename(source_path)
        remote_dest_path = self.full_path(destination_path)

        code = self.connection.root.file_move(path_to_file, file_name, remote_dest_path)
        return codes[code]

    def exposed_open_dir(self, path):
        remote_path = self.full_path(path)
        code, _ = self.connection.root.dir_read(remote_path)
        if code == 0:
            global CURRENT_DIR
            CURRENT_DIR.clear()
            CURRENT_DIR.append('/')
            for dir in remote_path.split('/'):
                if dir != '':
                    CURRENT_DIR.append(dir)

            return 'Current dfs path : \n' + os.path.join(*CURRENT_DIR)

        else:
            return codes[code]

    def exposed_read_dir(self, path):
        remote_path = self.full_path(path)
        code, content = self.connection.root.dir_read(remote_path)
        return content

    def exposed_make_dir(self, path):
        path_to_dir, dir_name = self.filepath_filename(path)
        code, dirname = self.connection.root.dir_create(path_to_dir, dir_name)
        return codes[code]

    def exposed_delete_dir(self, path):
        path_to_dir, dir_name = self.filepath_filename(path)
        code = self.connection.root.dir_delete(path_to_dir, dir_name)
        return codes[code]


if __name__ == '__main__':
    print('Starting dfs client...')
    t = ThreadedServer(Client, port=22222)
    t.start()
