import uuid
import os
import math
import pickle
import rpyc
import signal
from rpyc.utils.server import ThreadedServer

FS = {}
data_nodes = [{"ip": "datanode1", "port": 22800}, {"ip": "datanode2", "port": 22801}]
replica_factor = 2
last_node_id = 0
block_size = 16 * 2 ** 10


### Persistence of filesystem

def restore_data():
    global FS, last_node_id
    FS,last_node_id  = load_data()


def save_data(arg1, arg2):
    with open('dfs.pickle', 'wb') as f:
        pickle.dump(FS, last_node_id, f)
        print('[INFO] Data saved')


def load_data():
    data = {}
    last_node_id = 0
    try:
        with open('dfs.pickle', 'rb') as f:
            data, last_node_id = pickle.load(f)
        print('[INFO] Filesystem loaded')
    except:
        print('[WARN] No dump of filesystem')
    return data, last_node_id


### Helpers function

def get_folders_path(path):
    folders = path.split('/')
    if path == '/':
        return []
    return folders[1:]


def get_current_dir(path):
    dir_path = get_folders_path(path)
    pwd = FS
    for directory in dir_path:
        pwd = pwd[directory]
    return pwd


def create_block_id():
    return str(uuid.uuid1())


def is_file(content):
    try:
        is_file = (type(content['block']) == type([]))
    except KeyError:
        is_file = False

    return is_file


### DFS

def init():
    global FS
    FS = {}
    available_size = 20000000000
    code = -1
    for node in data_nodes:
        print('trying conntect to',node)
        try:
            c = rpyc.connect(node["ip"], node["port"])
            new_code, new_size = c.root.flush()
            print(new_size)
            available_size = min(available_size, new_size)
        except:
            new_code = 3
            print('cont connect to node', node)

        if (code != 0 and new_code == 0):
            code = 0
        elif code != 0:
            code = new_code

    # TODO: implement flush on datanode
    return code, available_size


### FILEs SECTION ###
def file_create(path, filename):
    pwd = get_current_dir(path)
    # block_name = uuid.uuid1()
    global last_node_id
    last_node_id += 1
    pwd[filename] = {'name': filename, 'node_id': last_node_id, 'size': 0, 'block': [], 'blocks_address': {}}
    # for node in data_nodes:
    #     write_byteblock(block_name, b'', node)
    return 0


def file_info(path, filename):
    print('[ACTION] Getting file info', filename)
    pwd = get_current_dir(path)
    is_dir = not is_file(pwd[filename])
    result = pwd[filename]
    code = 0
    if is_dir:
        code = 1
        result = []
        print('\t it is a dir')
    return code, result


def file_delete(path, filename):
    pwd = get_current_dir(path)
    file = pwd[filename]
    block_names = file['block']
    code = 1
    for name in block_names:
        addresses = file['blocks_address'][name]
        for address in addresses:
            new_code = delete_byteblock(name, address)

            if (code != 0 and new_code == 0):
                code = 0
            elif code != 0:
                code = new_code

    pwd.pop(filename, None)
    return code


def file_copy(path, filename, new_path):
    pwd = get_current_dir(path)
    file = pwd[filename]
    new_pwd = get_current_dir(new_path)
    file_create(new_path, filename)
    new_pwd[filename]['size'] = file['size']

    global last_node_id
    last_node_id += 1
    new_pwd[filename]['node_id'] = last_node_id

    block_names = file['block'].copy()

    code = -1
    for name in block_names:
        addresses = file['blocks_address'][name]
        new_name = create_block_id()
        for address in addresses:
            new_code = copy_byteblock(name, new_name, address)

            if (code != 0 and new_code == 0):
                code = 0
            elif code != 0:
                code = new_code

        print('2',new_pwd)
        print('2',pwd)
        new_pwd[filename]['block'].append(new_name)
        print('3',new_pwd)
        print('3',pwd)
        new_pwd[filename]['blocks_address'][new_name] = data_nodes
        print('5',new_pwd)
        print('5',pwd)

    return code


def file_move(path, filename, new_path):
    pwd = get_current_dir(path)
    new_pwd = get_current_dir(new_path)
    new_pwd[filename] = pwd.pop(filename, None)
    return 0


def file_read(path, filename):
    pwd = get_current_dir(path)
    file = pwd[filename]
    block_names = file['block']
    bfile = b''
    code = 0
    for name in block_names:
        addresses = file['blocks_address'][name]
        byteblock = None
        for address in addresses:
            new_code, byteblock = read_byteblock(name, address)

            if (code != 0 and new_code == 0):
                code = 0
            elif code != 0:
                code = new_code

            if byteblock == None:
                continue
            else:
                break

        if byteblock == None:
            print('[ERROR] No datablock like this on all namenodes:', name)
            code = 4
        bfile += byteblock
    return code, bfile


def file_write(path, filename, bfile):
    size = len(bfile)  # get size of file in bytes
    file_create(path, filename)  # create file in FS
    pwd = get_current_dir(path)
    file = pwd[filename]
    file['size'] = size
    num_blocks = math.ceil(size / block_size)  # get # of data blocks in file
    addresses = []
    code = -1
    for i in range(num_blocks):
        b_part = i * block_size
        if i + 1 == num_blocks:
            e_part = len(bfile)
        else:
            e_part = (i + 1) * block_size
        byteblock = bfile[b_part:e_part]
        uid = create_block_id()
        file['block'].append(uid)
        file['blocks_address'][uid] = data_nodes  # ХЗ возможно не робит как надо
        for node in data_nodes:
            addresses.append((uid, node))
            new_code = write_byteblock(uid, byteblock, node)

            if (code != 0 and new_code == 0):
                code = 0
            elif code != 0:
                code = new_code

    print('[INFO] Done writing file:',filename, size, 'code:', code)
    return code


def read_byteblock(name, address):
    print('trye to connect')
    byteblock = None
    try:
        c = rpyc.connect(address["ip"], address["port"])
        code, byteblock = c.root.read_block(name)
    except:
        print("Error connection")
        code = 3

    print('code:', code)
    return code, byteblock


def write_byteblock(name, byteblock, address):
    print('try to connect')
    try:
        datanode = rpyc.connect(address["ip"], address["port"])
        # datanode = rpyc.connect("datanode", 22800)
        code = datanode.root.write_block(name, byteblock)
        print('success')
    except:
        print("Error connection")
        code = 3
    print('code:', code)
    return code


def delete_byteblock(name, address):
    print('Deletion. Connecting to datanodes')
    try:
        datanode = rpyc.connect(address['ip'], address['port'])
        code = datanode.root.delete_block(name)
        print('success')
    except:
        print("Error connection")
        code = 3
    print('code:', code)
    return code


def copy_byteblock(name, new_name, address):
    print('Copying. Connecting to datanodes')
    try:
        datanode = rpyc.connect(address['ip'], address['port'])
        code = datanode.root.copy_block(name, new_name)
        print('success')
    except:
        print("Error connection")
        code = 3
    print('code:', code)
    return code


### DIRs SECTION ###

def dir_create(path, dir_name):
    pwd = get_current_dir(path)
    pwd[dir_name] = {}
    return 0


def dir_read(path):
    ls = []
    pwd = get_current_dir(path)
    is_dir = not is_file(pwd)
    code = 0
    if is_dir:
        for name in pwd:
            ls.append(name)
    else:
        code = 1
    return code, ls


def dir_delete(path, dir_name):
    print('[Action] Removing dir =', path + '/' + dir_name)
    pwd = get_current_dir(path)
    content = pwd[dir_name]
    items = list(content.keys())
    code = 0
    for item in items:
        print('\t has content:', item)
        # Check is it dir or file

        if path == '/':
            path = ''

        if is_file(content[item]):
            print('\t Have a file -> Removing', item)
            code = file_delete(path + '/' + dir_name, item)
        else:
            print('\t Have a dir -> Removing', item)
            code = dir_delete(path + '/' + dir_name, item)

    pwd.pop(dir_name, None)

    return code


class Namenode(rpyc.Service):
    def on_connect(self, conn):
        # code that runs when a connection is created
        # (to init the service, if needed)
        print('Connected:', conn)
        pass

    def exposed_get_fs(self):
        return str(FS) + '\n' + str(data_nodes)

    ### Functionality

    # inititialization

    def exposed_init(self):
        code = 0
        free_space = None
        try:
            code, free_space = init()
        except:
            code = 3

        return code, free_space

    # dirs

    def exposed_dir_create(self, path_to_dir, dir_name):
        code = 0
        try:
            dir_create(path_to_dir, dir_name)
        except KeyError:
            code = 2

        return code, dir_name


    def exposed_dir_read(self, path_to_dir):
        content = []
        try:
            code, content = dir_read(path_to_dir)
        except KeyError:
            code = 1

        return code, content

    def exposed_dir_delete(self, path_to_dir, dir_name):
        code = 0
        try:
            dir_delete(path_to_dir, dir_name)
        except KeyError:
            code = 1

        return code


    def exposed_file_create(self, path_to_file, file_name):
        code = 0
        try:
            file_create(path_to_file, file_name)
        except KeyError:
            code = 2

        return code

    def exposed_file_info(self, path_to_file, file_name):
        code = 0
        content = ""
        try:
            code, content = file_info(path_to_file, file_name)
        except KeyError:
            code = 1

        return code, str(content)

    def exposed_file_delete(self, path_to_file, file_name):
        code = 0
        try:
            file_delete(path_to_file, file_name)
        except KeyError:
            code = 1
        except:
            code = 3

        return code

    def exposed_file_copy(self, path_to_file, file_name, path_to_new_dir):
        code = 0
        try:
            file_copy(path_to_file, file_name, path_to_new_dir)
        except KeyError:
            code = 1
        except:
            code = 3

        return code

    def exposed_file_move(self, path_to_file, file_name, path_to_new_dir):
        code = 0
        try:
            file_move(path_to_file, file_name, path_to_new_dir)
        except KeyError:
            code = 1

        return code

    def exposed_file_write(self, path_to_dir, file_name, file_bytes):
        code = 0
        try:
            code = file_write(path_to_dir, file_name, file_bytes)
        except KeyError:
            code = 1
        except:
            code = 3

        return code

    def exposed_file_read(self, path_to_dir, file_name):
        code = 0
        bfile = b''
        try:
            code, bfile = file_read(path_to_dir, file_name)
        except KeyError:
            code = 1
        except:
            code = 3

        return code, bfile

    def exposed_test_conn(self):
        dn = rpyc.connect("datanode", 22800)
        code = dn.root.write_block('test.write', b'kek')
        return code


if __name__ == '__main__':
    print('Starting namenode...')
    restore_data()
    signal.signal(signal.SIGINT, save_data)
    # signal.signal(9, save_data)
    t = ThreadedServer(Namenode, port=55555)
    t.start()
