import os
import rpyc


# API
def initialize():
    conection = rpyc.connect("localhost", 22222)
    block_size = conection.root.initialize()
    print(f'Success!\nAvailable size :{block_size}')


def create_file(path):
    conection = rpyc.connect("localhost", 22222)
    print(conection.root.create_file(path))


def read_file(path):
    conection = rpyc.connect("localhost", 22222)
    code, result = conection.root.read_file(path)
    if code == "success":
        with open(path.split('/')[-1], 'wb') as f:
            f.write(result)
    else:
        print(code)


def write_file(local_path, remote_path):
    conection = rpyc.connect("localhost", 22222)
    print(conection.root.write_file(local_path, remote_path))


def delete_file(path):
    conection = rpyc.connect("localhost", 22222)
    print(conection.root.delete_file(path))


def get_file_info(path):
    conection = rpyc.connect("localhost", 22222)
    result = conection.root.get_file_info(path)
    print(result)


def copy_file(source_path, dest_path):
    conection = rpyc.connect("localhost", 22222)
    print(conection.root.copy_file(source_path, dest_path))


def move_file(source_path, destination_path):
    conection = rpyc.connect("localhost", 22222)
    print(conection.root.move_file(source_path, destination_path))


def open_dir(path):
    conection = rpyc.connect("localhost", 22222)
    print(conection.root.open_dir(path))


def read_dir(path):
    conection = rpyc.connect("localhost", 22222)
    files = conection.root.read_dir(path)
    print('    '.join(files))


def make_dir(path):
    conection = rpyc.connect("localhost", 22222)
    print(conection.root.make_dir(path))


def delete_dir(path):
    conection = rpyc.connect("localhost", 22222)
    print(conection.root.delete_dir(path))
