import shutil
import rpyc
import os
import signal
from rpyc.utils.server import ThreadedServer


dest = "./storage"

class Datanode(rpyc.Service):
    def on_connect(self, conn):
        # code that runs when a connection is created
        # (to init the service, if needed)
        print("Conected: ", conn)


    def exposed_free_disk_space(self):
        return show_free_disk_space()


    def exposed_write_block(self, name, block):
        code = 0
        try:
            write_block(name, block)
        except:
            code = 1
        return code


    def exposed_read_block(self, name):
        code = 0
        content = []
        try:
            content = read_block(name)
        except KeyError:
            code = 1
        return code, content


    def exposed_delete_block(self, name):
        code = 0
        try:
            delete_block(name)
        except KeyError:
            code = 1
        return code


    def exposed_copy_block(self, prev_name, new_name):
        code = 0
        try:
            copy_block(prev_name, new_name)
        except KeyError:
            code = 1
        return code


    def exposed_flush(self):
        code = 0
        free_space = show_free_disk_space()
        try:
            free_space = delete_everything()
        except KeyError:
            code = 4
        return code, free_space


def show_free_disk_space():
    total, used, free = shutil.disk_usage("/")
    print("Free: %d MiB" % (free // (2 ** 20)))
    return free // (2 ** 20)


def write_block(name, block):
    print('try to write block')
    f = open(dest + '/' + name, 'wb')
    f.write(block)
    f.close()
    return 0


def read_block(name):
    f = open(dest + '/' + name, 'rb')
    block = f.read()
    f.close()
    return block


def delete_block(name):
    if os.path.exists(dest + '/' + name):
        os.remove(dest + '/' + name)
    else:
        print("The file does not exist")
    return 0

def copy_block(prev_block_name, new_block_name):
    f1 = open(dest + '/' + prev_block_name, 'rb')
    block = f1.read()
    f2 = open(dest + '/' + new_block_name, 'wb')
    f2.write(block)
    f2.close()
    f1.close()


def delete_everything():
    # dest = "/storage"
    os.makedirs(dest, exist_ok=True)
    # src = os.getcwd()
    # shutil.copytree(src, dest)
    o = os.listdir(dest)
    for filename in o:
        os.unlink(dest + '/' + filename)
    return show_free_disk_space()

if __name__ == "__main__":
    print("Starting datanode")
    # signal.signal(signal.SIGINT,int_handler)
    t = ThreadedServer(Datanode, port=int(os.environ['NODE_PORT']))
    t.start()
