import sys

sys.path.append('/app')
sys.path.append('/app/src')
from src import client


def initialize():
    client.initialize()


def create_file(path):
    client.create_file(path)


def read_file(path):
    client.read_file(path)


def write_file(local_path, remote_path):
    client.write_file(local_path, remote_path)


def delete_file(path):
    client.delete_file(path)


def get_file_info(path):
    client.get_file_info(path)


def copy_file(source_path, dest_path):
    client.copy_file(source_path, dest_path)


def move_file(source_path, destination_path):
    client.move_file(source_path, destination_path)


def open_dir(path):
    client.open_dir(path)


def read_dir(path):
    client.read_dir(path)


def make_dir(path):
    client.make_dir(path)


def delete_dir(path):
    client.delete_dir(path)
