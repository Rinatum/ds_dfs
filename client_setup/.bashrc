# ~/.bashrc: executed by bash(1) for non-login shells.

# Note: PS1 and umask are already set in /etc/profile. You should not
# need this unless you want different defaults for root.
# PS1='${debian_chroot:+($debian_chroot)}\h:\w\$ '
# umask 022

# You may uncomment the following lines if you want `ls' to be colorized:
# export LS_OPTIONS='--color=auto'
# eval "`dircolors`"
# alias ls='ls $LS_OPTIONS'
# alias ll='ls $LS_OPTIONS -l'
# alias l='ls $LS_OPTIONS -lA'
#
# Some more alias to avoid making mistakes:
# alias rm='rm -i'
# alias cp='cp -i'
# alias mv='mv -i'
# Custom aliases
alias погнали='python -c "import sys; sys.path.append(\"/\"); from shortcuts import *; client.initialize()"'
alias создать_файлик='python -c \
"import sys;\
sys.path.append(\"/\");\
from shortcuts import *;\
num_of_args = 1;\
assert (len(sys.argv) == num_of_args + 1),\"Wrong number of arguments\";\
client.create_file(sys.argv[1])" $1 '

alias читать_файлик='python -c \
"import sys;\
sys.path.append(\"/\");\
from shortcuts import *;\
num_of_args = 1;\
assert (len(sys.argv) == num_of_args + 1),\"Wrong number of arguments\";\
client.read_file(sys.argv[1])" $1 '

alias писать_файлик='python -c \
"import sys;\
sys.path.append(\"/\");\
from shortcuts import *;\
num_of_args = 2;\
assert (len(sys.argv) == num_of_args + 1),\"Wrong number of arguments\";\
client.write_file(sys.argv[1], sys.argv[2])" $1 $2'

alias убить_файлик='python -c \
"import sys;\
sys.path.append(\"/\");\
from shortcuts import *;\
num_of_args = 1;\
assert (len(sys.argv) == num_of_args + 1),\"Wrong number of arguments\";\
client.delete_file(sys.argv[1])" $1 '

alias че_за_файлик='python -c \
"import sys;\
sys.path.append(\"/\");\
from shortcuts import *;\
num_of_args = 1;\
assert (len(sys.argv) == num_of_args + 1),\"Wrong number of arguments\";\
client.get_file_info(sys.argv[1])" $1 '

alias копи_файлик='python -c \
"import sys;\
sys.path.append(\"/\");\
from shortcuts import *;\
num_of_args = 2;\
assert (len(sys.argv) == num_of_args + 1),\"Wrong number of arguments\";\
client.copy_file(sys.argv[1], sys.argv[2])" $1 $2 '

alias сдвинуть_файлик='python -c \
"import sys;\
sys.path.append(\"/\");\
from shortcuts import *;\
num_of_args = 2;\
assert (len(sys.argv) == num_of_args + 1),\"Wrong number of arguments\";\
client.move_file(sys.argv[1], sys.argv[2])" $1 $2 '

alias идти_в_папку='python -c \
"import sys;\
sys.path.append(\"/\");\
from shortcuts import *;\
num_of_args = 1;\
assert (len(sys.argv) == num_of_args + 1),\"Wrong number of arguments\";\
client.open_dir(sys.argv[1])" $1 '

alias че_в_папке='python -c \
"import sys;\
sys.path.append(\"/\");\
from shortcuts import *;\
num_of_args = 1;\
assert (len(sys.argv) == num_of_args + 1),\"Wrong number of arguments\";\
client.read_dir(sys.argv[1])" $1 '

alias создать_папку='python -c \
"import sys;\
sys.path.append(\"/\");\
from shortcuts import *;\
num_of_args = 1;\
assert (len(sys.argv) == num_of_args + 1),\"Wrong number of arguments\";\
client.make_dir(sys.argv[1])" $1 '

alias убить_папку='python -c \
"import sys;\
sys.path.append(\"/\");\
from shortcuts import *;\
num_of_args = 1;\
assert (len(sys.argv) == num_of_args + 1),\"Wrong number of arguments\";\
client.delete_dir(sys.argv[1])" $1 '